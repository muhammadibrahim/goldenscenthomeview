package com.ibrahim.muhammad.goldenscenthomeview.utils;

import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.DataModel;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public interface GridItemClickListener {
    void onGridItemClick(DataModel model);
}
