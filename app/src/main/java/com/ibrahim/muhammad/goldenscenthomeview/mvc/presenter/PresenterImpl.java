package com.ibrahim.muhammad.goldenscenthomeview.mvc.presenter;

import com.ibrahim.muhammad.goldenscenthomeview.R;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.DataModel;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.TopItemsDataModel;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.view.IMainView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public class PresenterImpl implements IPresenter {

    IMainView mainView;

    public PresenterImpl(IMainView mainView){
        this.mainView = mainView;
    }
    @Override
    public List<DataModel> initLipsData() {
        List<DataModel> lipsData = new ArrayList<>();
        lipsData.add(new DataModel("Pencil", R.drawable.pencil));
        lipsData.add(new DataModel("Lipstick", R.drawable.lipstick));
        lipsData.add(new DataModel("Lipgloss", R.drawable.lipgloss));
        lipsData.add(new DataModel("Lip Balm", R.drawable.lip_balm));
        lipsData.add(new DataModel("Treatment", R.drawable.treatment));
        lipsData.add(new DataModel("Palette", R.drawable.palette));
        return lipsData;
    }

    @Override
    public List<DataModel> initFaceData() {
        List<DataModel> faceData = new ArrayList<>();
        faceData.add(new DataModel("Mat Touch", R.drawable.mat_touch));
        faceData.add(new DataModel("Liquid Soap", R.drawable.liquid_soap));
        faceData.add(new DataModel("Blusher", R.drawable.makup_blusher));
        faceData.add(new DataModel("Cream", R.drawable.makup_cream));
        faceData.add(new DataModel("Powder", R.drawable.powder));
        faceData.add(new DataModel("Moisturizer", R.drawable.cream_moisturizing));
        return faceData;
    }

    @Override
    public List<DataModel> initNailsData() {
        List<DataModel> nailsData = new ArrayList<>();
        nailsData.add(new DataModel("Pencil", R.drawable.pencil));
        nailsData.add(new DataModel("Lipstick", R.drawable.lipstick));
        nailsData.add(new DataModel("Lipgloss", R.drawable.lipgloss));
        nailsData.add(new DataModel("Lip Balm", R.drawable.lip_balm));
        nailsData.add(new DataModel("Treatment", R.drawable.treatment));
        nailsData.add(new DataModel("Palette", R.drawable.palette));
        return nailsData;
    }

    @Override
    public List<TopItemsDataModel> initTopItemsData() {
        List<TopItemsDataModel> topItemsData = new ArrayList<>();
        topItemsData.add(new TopItemsDataModel("Montale", "some description of the item.", "699 SR" ,R.drawable.montale_perfumes));
        topItemsData.add(new TopItemsDataModel("Salvatore Ferragamo", "some description of the item.", "500 SR" ,R.drawable.salvatore_ferragamo_perfumes));
        topItemsData.add(new TopItemsDataModel("Sofia Vergara", "some description of the item.", "299 SR" ,R.drawable.sofia_vergara_perfumes));
        topItemsData.add(new TopItemsDataModel("Chantal Thomass", "some description of the item.", "350 SR" ,R.drawable.chantal_thomass_perfume));
        topItemsData.add(new TopItemsDataModel("Tory Burch", "some description of the item.", "700 SR" ,R.drawable.tory_burch_perfumes));
        topItemsData.add(new TopItemsDataModel("Vercace", "some description of the item.", "400 SR" ,R.drawable.vercace_perfume));
        topItemsData.add(new TopItemsDataModel("Vince Camuto", "some description of the item.", "300 SR" ,R.drawable.vince_camuto_perfume));
        topItemsData.add(new TopItemsDataModel("Vince Camuto", "some description of the item.", "550 SR" ,R.drawable.vince_camuto_perfume_1));
        topItemsData.add(new TopItemsDataModel("Lancome Tresor", "some description of the item.", "499 SR" ,R.drawable.lancome_tresor_perfume));
        return topItemsData;
    }
}
