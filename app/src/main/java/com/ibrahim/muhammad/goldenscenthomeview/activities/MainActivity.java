package com.ibrahim.muhammad.goldenscenthomeview.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ibrahim.muhammad.goldenscenthomeview.R;
import com.ibrahim.muhammad.goldenscenthomeview.adapters.TopItemsRecyclerviewAdapter;
import com.ibrahim.muhammad.goldenscenthomeview.custom_view.CustomExpandableView;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.DataModel;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.presenter.IPresenter;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.presenter.PresenterImpl;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.view.IMainView;
import com.ibrahim.muhammad.goldenscenthomeview.utils.FooterClickListener;
import com.ibrahim.muhammad.goldenscenthomeview.utils.GridItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IMainView {

    IPresenter presenter;

    @BindView(R.id.lipsView)
    CustomExpandableView lipsView;

    @BindView(R.id.faceView)
    CustomExpandableView faceView;

    @BindView(R.id.nailsView)
    CustomExpandableView nailsView;

    @BindView(R.id.llSideView)
    LinearLayout llSideView;

    @BindView(R.id.cardView1)
    CardView cardView;

    @BindView(R.id.topItemsList)
    RecyclerView topItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        presenter = new PresenterImpl(this);

        lipsView.setData(presenter.initLipsData());
        lipsView.setGridItemClickListener(gridItemClickListener);
        lipsView.setFooterClickListener(footerClickListener);


        faceView.setData(presenter.initFaceData());
        faceView.setGridItemClickListener(gridItemClickListener);
        faceView.setFooterClickListener(footerClickListener);


        nailsView.setData(presenter.initNailsData());
        nailsView.setGridItemClickListener(gridItemClickListener);
        nailsView.setFooterClickListener(footerClickListener);


        TopItemsRecyclerviewAdapter adapter = new TopItemsRecyclerviewAdapter(this, presenter.initTopItemsData());
        topItemsList.setLayoutManager(new GridLayoutManager(this, 3, LinearLayoutManager.HORIZONTAL, false));
        topItemsList.setHasFixedSize(true);
        topItemsList.setAdapter(adapter);

    }


    //region click listeners
    GridItemClickListener gridItemClickListener = new GridItemClickListener() {
        @Override
        public void onGridItemClick(DataModel model) {
            Toast.makeText(MainActivity.this, model.getTitle()+" clicked", Toast.LENGTH_LONG).show();
        }
    };

    FooterClickListener footerClickListener = new FooterClickListener() {
        @Override
        public void onFooterClick(View view) {
            Toast.makeText(MainActivity.this, "View more clicked", Toast.LENGTH_LONG).show();
        }
    };
    //endregion

}
