package com.ibrahim.muhammad.goldenscenthomeview.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ibrahim.muhammad.goldenscenthomeview.R;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.TopItemsDataModel;

import java.util.List;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public class TopItemsRecyclerviewAdapter extends RecyclerView.Adapter<TopItemsRecyclerviewAdapter.TopItemViewHolder>  {

    Context context;
    List<TopItemsDataModel> data;

    public TopItemsRecyclerviewAdapter(Context context, List<TopItemsDataModel> data){
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public TopItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top_product, parent, false);
        return new TopItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopItemViewHolder holder, int position) {
        final TopItemsDataModel dataModel = data.get(position);
        holder.itemImage.setImageResource(dataModel.getPic_id());
        holder.price.setText(dataModel.getPrice());
        holder.description.setText(dataModel.getDescription());
        holder.title.setText(dataModel.getTitle());

        holder.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Add "+dataModel.getTitle()+" to cart!", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class TopItemViewHolder extends RecyclerView.ViewHolder{

        ImageView itemImage;
        TextView title;
        TextView description;
        TextView price;
        Button addBtn;

        public TopItemViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.itemImage);
            title = itemView.findViewById(R.id.itemTitle);
            description = itemView.findViewById(R.id.itemDesc);
            price = itemView.findViewById(R.id.itemPrice);
            addBtn = itemView.findViewById(R.id.addBtn);
        }
    }
}
