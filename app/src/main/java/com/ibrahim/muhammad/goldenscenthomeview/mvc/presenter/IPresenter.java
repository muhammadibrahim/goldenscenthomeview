package com.ibrahim.muhammad.goldenscenthomeview.mvc.presenter;

import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.DataModel;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.TopItemsDataModel;

import java.util.List;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public interface IPresenter {
    List<DataModel> initLipsData();
    List<DataModel> initFaceData();
    List<DataModel> initNailsData();
    List<TopItemsDataModel> initTopItemsData();
}
