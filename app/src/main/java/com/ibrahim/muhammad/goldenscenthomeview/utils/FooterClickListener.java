package com.ibrahim.muhammad.goldenscenthomeview.utils;

import android.view.View;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public interface FooterClickListener {
    void onFooterClick(View view);
}
