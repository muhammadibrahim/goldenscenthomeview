package com.ibrahim.muhammad.goldenscenthomeview.mvc.model;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public class TopItemsDataModel {

    private String title;
    private String description;
    private String price;
    private int pic_id;

    public TopItemsDataModel(String title, String description, String price, int pic_id){
        this.title = title;
        this.description = description;
        this.price = price;
        this.pic_id = pic_id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public int getPic_id() {
        return pic_id;
    }
}
