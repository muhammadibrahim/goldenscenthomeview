package com.ibrahim.muhammad.goldenscenthomeview.mvc.model;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public class DataModel {

    private String title;
    private int pic_id;

    public DataModel(String title, int pic_id){
        this.title = title;
        this.pic_id = pic_id;
    }


    public int getPic_id() {
        return pic_id;
    }

    public String getTitle() {
        return title;
    }
}
