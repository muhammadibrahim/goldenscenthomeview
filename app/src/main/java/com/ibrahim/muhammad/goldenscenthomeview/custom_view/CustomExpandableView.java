package com.ibrahim.muhammad.goldenscenthomeview.custom_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibrahim.muhammad.goldenscenthomeview.R;
import com.ibrahim.muhammad.goldenscenthomeview.mvc.model.DataModel;
import com.ibrahim.muhammad.goldenscenthomeview.utils.FooterClickListener;
import com.ibrahim.muhammad.goldenscenthomeview.utils.GridItemClickListener;

import java.util.List;

/**
 * Created by muhammadibrahim on 7/14/18.
 */

public class CustomExpandableView extends LinearLayout implements View.OnClickListener {

    private int width;
    private int height;

    private TextView tvHeaderTitle;
    private TextView tvFooterTitle;
    private GridView gridView;

    private boolean isExpanded;
    private String headerTitle;
    private String footerTitle;

    private GridItemClickListener gridItemClickListener;
    private FooterClickListener footerClickListener;

    Context context;

    public CustomExpandableView(Context context) {
        super(context);
        init(context, null);
    }

    public CustomExpandableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.custom_expandable_view, this);
        getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);

        loadViews();

        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomExpandableView, 0,0);
        try{
            isExpanded = array.getBoolean(R.styleable.CustomExpandableView_isExpanded, false);
            headerTitle = array.getString(R.styleable.CustomExpandableView_headerTitle);
            footerTitle = array.getString(R.styleable.CustomExpandableView_footerTitle);
            initViews();
        }
        finally {
            array.recycle();
        }
    }

    private void loadViews() {
        tvFooterTitle = findViewById(R.id.footer_title);
        tvHeaderTitle = findViewById(R.id.header_title);
        gridView = findViewById(R.id.gridview);
    }

    private void initViews() {
        tvHeaderTitle.setText(headerTitle);
        tvFooterTitle.setText(footerTitle);
        gridView.setVisibility(isExpanded?VISIBLE:GONE);
        tvFooterTitle.setVisibility(isExpanded?VISIBLE:GONE);

        if(isExpanded){
            tvHeaderTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.icon_collapse, 0);
        }
        else{
            tvHeaderTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.icon_expand, 0);
        }

        tvHeaderTitle.setOnClickListener(this);
        tvFooterTitle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.header_title:
                if(isExpanded()){
                    setExpanded(false);
                }
                else{
                    setExpanded(true);
                }
                break;
            case R.id.footer_title:
                footerClickListener.onFooterClick(v);
                break;
        }
        super.callOnClick();
    }


    //region getters & setters
    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
        gridView.setVisibility(isExpanded?VISIBLE:GONE);
        tvFooterTitle.setVisibility(isExpanded?VISIBLE:GONE);
        if(isExpanded){
            tvHeaderTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.icon_collapse, 0);
        }
        else{
            tvHeaderTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.icon_expand, 0);
        }
        invalidate();
        requestLayout();
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
        invalidate();
        requestLayout();
    }

    public String getFooterTitle() {
        return footerTitle;
    }

    public void setFooterTitle(String footerTitle) {
        this.footerTitle = footerTitle;
        invalidate();
        requestLayout();
    }

    //endregion


    ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            width = getWidth() - 30;
            height = getHeight();

            Log.d("view_dimen", "WxH = "+width+" x "+height);
        }
    };

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);

        ViewGroup.LayoutParams params = getLayoutParams();
        params.height = getMeasuredHeight();
    }


    public void setGridItemClickListener(GridItemClickListener listener){
        this.gridItemClickListener = listener;
    }

    public void setFooterClickListener(FooterClickListener listener){
        this.footerClickListener = listener;
    }


    public void setData(List<DataModel> data){
        MyGridViewAdapter adapter = new MyGridViewAdapter(context, data);
        gridView.setAdapter(adapter);
    }

    class MyGridViewAdapter extends BaseAdapter {

        private Context mContext;
        private List<DataModel> data;

        public MyGridViewAdapter(Context c, List<DataModel> data) {
            mContext = c;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gridview, parent, false);
                viewHolder.imageView = convertView.findViewById(R.id.itemImage);
                viewHolder.title = convertView.findViewById(R.id.itemTitle);

                convertView.setTag(viewHolder);
            }
            else
            {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final DataModel dataModel = data.get(position);
            viewHolder.title.setText(dataModel.getTitle());
            viewHolder.imageView.setImageResource(dataModel.getPic_id());
            viewHolder.imageView.setLayoutParams(new LayoutParams(width/3, width/3));

            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        gridItemClickListener.onGridItemClick(dataModel);
                    }
                    catch (Exception e){}
                }
            });

            return convertView;
        }


        public class ViewHolder{
            ImageView imageView;
            TextView title;
        }
    }
}
